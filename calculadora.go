package mycalculator

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Calc struct{}

/* | Le pasamos la structura esto se llama recive function, poniendo los parentesis
le pasamos el struct que acabamos de crear, asi le damos la propiedad a calc de
tener este metodo dentro de el, es decir llamamos cal.operate*/
func (Calc) Operate(intro string, operator string) int {
	cleanentrance := strings.Split(intro, operator)
	operator1 := parser(cleanentrance[0])
	operator2 := parser(cleanentrance[1])

	switch operator {
	case "+":
		fmt.Println(operator1 + operator2)
		return operator1 + operator2
	case "-":
		fmt.Println(operator1 - operator2)
		return operator1 - operator2
	case "*":
		fmt.Println(operator1 * operator2)
		return operator1 * operator2
	case "/":
		fmt.Println(operator1 / operator2)
		return operator1 / operator2
	default:
		fmt.Println(operator, `operador no esta soportado`)
		return 0
	}
}

//Creamos una funcion nueva que se llama parsear

func parser(intro string) int {
	/* Vamos a castear los valores para poderlos sumar porque siguen siendo string
		   Usamos atoi que nos permite tomar un string convertirlo a un entero, Atoi es
		   una funcion que nos retorna 2 valores usamos quion bajo que nos permite crear
		   un whilecard en la cual no tenemos que utilizar esta variable

		   Sirve como placeholder anónimo.
		   Por ejemplo, si tengo una function que devuelve 3 valores pero nadamas me interesa uno de ellos,
		   haria algo asi:

		   _, y, _ := getxyz()```
	       para obtener el valor de y.  */
	//operador1, _ := strconv.Atoi(valores[0])
	operador, _ := strconv.Atoi(intro)
	return operador
}

func Readintro() string {
	scan := bufio.NewScanner(os.Stdin)
	scan.Scan()
	return scan.Text()
}
